rm(list = ls())

path = "~/Dropbox/FlexiMC_Data/Gen1_Data/[IN-050C]/2019/2019-01"

days = dir(path)

c1 = c2 = c()

for(x in 1:length(days))
{
	newpath = paste(path,days[x],sep="/")
	files = dir(newpath)
	c1[x] = days[x]
	c2[x] = length(files)
}

data = data.frame(Stn=c1,Files=c2)

write.table(data,file="/tmp/stat.txt",row.names=F,col.names=F,sep="\t")
