rm(list=ls())
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-009T]"')
source('/home/admin/CODE/IN009Digest/2G3GFunctions.R')
require('mailR')
errHandle = file('/home/admin/Logs/LogsIN009Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}
daysAlive = 0
DOB = "15-01-2016"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = c('andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com',
'rohit.jaswal@cleantechsolar.com','abhishek.puri@cleantechsolar.com')
pwd = 'CTS&*(789'

sendMail= function(path)
{
	dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	filenams = paste(currday,".txt",sep="")
	body = " "
	body = paste(body,"\n\nSite Name: Becton Dickinson")
	body = paste(body,"\n\nLocation: Delhi, India")
	body = paste(body,"\n\nO&M Code: IN-009")
	body = paste(body,"\n\nSystem Size: 128.5")
	body = paste(body,"\n\nNumber of Energy Meters: 1")
	body = paste(body,"\n\nModule Brand / Model / Nos: REC / 260W / 72 & REC / 305W / 360")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / FLX17 / 7")
	body = paste(body,"\n\nSite COD: 2016-01-25")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(daysAlive)-10)))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(daysAlive)-10)/365,2)))
	bodyac = body
	body =  " "
	TOTALGENCALC = 0
	MYLD = c()
	
	body = "\n\n------------------------------------------------\n\n"
	body = paste(body,"\t\t",currday)
	body = paste(body,"\n\n------------------------------------------------\n\n")
	body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
	body = paste(body,"Irradiation [kWh/m2]:",as.character(dataread[1,3]),"\n\n")
	body = paste(body,"EAC method-1(Pac) [kWh]:",as.character(dataread[1,4]),"\n\n")
	body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,5]),"\n\n")
	body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
	body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,7]),"\n\n")
	body = paste(body,"PR-1 [%]:",as.character(dataread[1,8]),"\n\n")
	body = paste(body,"PR-2 [%]:",as.character(dataread[1,9]),"\n\n")
	body = paste(body,"Last recorded energy meter reading [kWh]:",as.character(dataread[1,12]),"\n\n")
	body = paste(body,"Last recorded timestamp:",as.character(dataread[1,13]),"\n\n")
	body = paste(body,"Mean Tmod [C]:",as.character(dataread[1,10]),"\n\n")
	body = paste(body,"Mean Tmod Solar Hours [C]:",as.character(dataread[1,11]),"\n\n")
	body = paste(body,"Station DOB:",as.character(DOB),"\n\n")
	body = paste(body,"Days alive:",as.character(daysAlive),"\n\n")
	body = paste(body,"\n\n------------------------------------------------\n\n")
	TOTALGENCALC = as.numeric(dataread[1,5])
	MYLD = as.numeric(dataread[1,7])
	bodyac = paste(bodyac,"\n\nSystem Full Generation (kWh):",TOTALGENCALC)
	bodyac = paste(bodyac,"\n\nSystem Full Yield (kWh/kWp):",MYLD)
	bodyac = paste(bodyac,body)
	body = bodyac

  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-009T] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = path,
            file.names = filenams, # optional paramete
            debug = F)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-009T]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-009T]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-009T]'

checkdir(path2G)
checkdir(path3G)

years = dir(path)
for(x in 1 : length(years))
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	path3Gyr = paste(path3G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	checkdir(path3Gyr)
	months = dir(pathyr)
	for(y in 1 : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		path3Gfinal = paste(path3Gyr,"/",months[y],".txt",sep = "")
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		days = dir(pathmon)
		if(length(days) > 0)
		{
		for(z in 1 : length(days))
		{
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon,days[z],sep="/")
			secondGenData(pathfinal,path2Gfinal)
			thirdGenData(path2Gfinal,path3Gfinal)
			daysAlive = daysAlive+1
		}
		}
	}
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
while(1)
{
	years = dir(path)
	noyrs = length(years)
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		path3Gyr = paste(path3G,years[x],sep="/")
		checkdir(path2Gyr)
		checkdir(path3Gyr)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			path3Gfinal = paste(path3Gyr,"/",mons[y],".txt",sep="")
			days = dir(pathmon)
			chkcopydays = days[grepl('Copy',days)]
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are',idxflse))
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			if(y > startmn)
			{
				prevz = z = 1
			}
			nodays = length(days) 
			if(prevz <= nodays)
			{
			for(z in prevz : nodays)
			{
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				pathdays = paste(pathmon,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal)
				thirdGenData(path2Gfinal,path3Gfinal)
				print('Sending mail')
				daysAlive = daysAlive+1
				sendMail(path2Gfinal)
			}
			}
		}
	}
	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
