rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .005
WAITFORNEWFILE = 0 # Flag to check if wait is needed to get PRData. This flag
                   # is set only by MailDigest.R. Idea is to wait for 711
									 # second gen data only when processing live
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
getPRData = function(df)
{
	gsi=PR1=PR2=NA
	date = as.character(df[1,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Cleantechsolar/1min/[711]/',yr,"/",mon,"/[711] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if(!file.exists(path) && WAITFORNEWFILE)
		Sys.sleep(7200)
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t",stringsAsFactors=F)
	gsi = as.numeric(dataread[1,3])
	PR1 = round(as.numeric(df[,6])*100/gsi,1)
	PR2 = round(as.numeric(df[,7])*100/gsi,1)
	}
	array2 = c(gsi,PR1,PR2)
	return(array2)
}

secondGenData = function(filepath,writefilepath)
{
	TIMESTAMPSALARM <<- NULL
  dataread = read.table(filepath,header = T,sep = "\t")
	if(nrow(dataread) < 1)
	{
	  print('Err in file')
		return(NULL)
	}
	dataread2 = dataread
	dataread = dataread2[complete.cases(dataread2[,2]),]
	{
	if(nrow(dataread) < 1)
	{
	 Eac2 = 0
	}
	else{
	Eac2 = format(round(as.numeric(dataread[nrow(dataread),2]) - as.numeric(dataread[1,2]),1),nsmall=1)
	}
	}
	dataread = dataread2[complete.cases(dataread2[,9]),]
  {
	if(nrow(dataread) < 1){
	 Eac1 = 0}
	else{
	Eac1 = format(round(sum(as.numeric(dataread[,9]))/12,1),nsmall=1)
	}
	}
	dataread = dataread2
	  LR=as.numeric(dataread[nrow(dataread),2])
  LT=dataread[nrow(dataread),1]
  DA = format(round(nrow(dataread)/2.88,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 479,]
  tdx = tdx[tdx > 479]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(dataread2[,9]),]
  missingfactor = 108 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,9]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
			TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
	dspy1 = round(as.numeric(Eac1)/106,2)
	dspy2 = round(as.numeric(Eac2)/106,2)
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/1.2,1),nsmall=1)
  df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield1=dspy1,DailySpecYield2=dspy2,stringsAsFactors=F)
  PRData = getPRData(df)
  df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield1=dspy1,DailySpecYield2=dspy2,
									IrrIN003T = PRData[1],PR1IN003T=PRData[2],PR2IN003T=PRData[3],LastRead = LR,LastTime = LT,stringsAsFactors=F)
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t")
  Eac1T = as.numeric(dataread1[,2])
  Eac2T = as.numeric(dataread1[,3])
  df = dataread1
	{
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
  return(c(as.numeric(Eac1T),as.numeric(Eac2T)))
}

