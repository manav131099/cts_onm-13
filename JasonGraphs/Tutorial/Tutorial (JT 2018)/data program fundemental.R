#
# VISUAL SETTINGS FOR GRAPH 
#
# BASIC FUNCTIONS FOR DATA ANALYSIS
#
# some help ---- kWh/m� (interpunct) day
# for standardization - size of text in plot : 3.6 to 4
# x/y axis text size : 11
# plot title and subtitle size : 12
# typical wall graph format: width 7.92 height 5 | landscape
# sum of daily active power generated over time = energy generated --- sum(pac)/60 = energy [kwh]
# calculate pr method 1 -> difference in eac/stationlimits/sum of gsi * 60000
#
# cbind - turn everything into factor; very troublesome!
#coord_cartesian(ylim = c(0,9))
rm(list=ls(all =TRUE))
library(ggplot2)

#

themesettings1 <- theme(plot.title = element_text(hjust = 0.5),                #title alignment
                        axis.title.x = element_text(size=19),                  # x axis label size
                        axis.title.y = element_text(size=19),                  # y axis label size
                        axis.text = element_text(size=11),                     # axes text size
                        panel.grid.minor = element_blank(),                    #remove minor grid lines
                        panel.border = element_blank(),                        #remove plot border
                        axis.line = element_line(colour = "black"),            # axis line colour
                        legend.justification = c(1, 1),                        #legend in plot
                        legend.position = c(1, 1))                             #legend position x,y

#ggplot title and subtitle settings
theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
      plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))


#break datetime variable 
xlabelscale <- scale_x_date(date_breaks = "1 month",date_labels = "%b")   

#break date time variable for 1-day timeseries
xlabelscale2 <- scale_x_continuous(breaks = c(1,361,721,1081,1440), labels = c("00:00","06:00","12:00","18:00","23:59"))

#break continuous x variable
scale_x_continuous(minor_breaks = seq(0,1200,400), breaks = seq(0,1200,200))

#dual-colour graph setting
plotscale_colour <-  scale_colour_manual('Period', values = c("Peak"="red", "Off-Peak"="lightblue"))      

#insert text into graph
text_insert <- annotate('text', label = "30-d moving average of PR", y = 26 , x = as.Date(result[101,1]), colour = "red")

#insert arrow shape into graph
arrow_insert <- annotate("segment", x = as.Date(result[110,1]), xend = as.Date(result[110,1]), y = 28, yend = 52 , colour = "red", size=1.5, alpha=0.5, arrow=arrow())

#moving average function
library(zoo)
ma <- function(x,y,del)
{
  v1 <- zoo(x,as.Date(y))
  v2 <- rollapplyr(v1, (-(del:1)), mean, fill = NA, na.rm = TRUE)
  return(v2)
}

###  E.G.1
#zoo.pr1 <- zoo(result[,7], as.Date(result$date))
#ma1 <- rollapplyr(zoo.pr1,list(-(29:1)), mean, fill=NA, na.rm=T)
#result$ambpr1.av=coredata(ma1)

### E.G.2

#result[,3][abs(result[,3])>5] <- NA
#ma1 <- ma(result[,3], result[,1], 365)
#result$mov_avg =  coredata(ma1)

#clgraph2 <- ggplot(result)+ylab("Cable Loss [%]")
#clFinalGraph2<- clgraph2 + geom_point(aes(x=date,y=cablem2), size=0.5)+
#  theme_bw() + geom_line(aes(x=date, y=mov_avg), size =1, colour = "red") 

clFinalGraph2

#y limit setting for plot
coord_cartesian(ylim = c(0,100))

#data smoothing 
p1 <- stat_smooth(colour="darkblue", method="loess", se =FALSE)
p1 <- geom_smooth()

#data binning example, can see more in clipping folder
zz <- round(as.numeric(as.character(dffg[,2])),0)
zz <- as.data.frame(zz[zz>5])

cuts <- apply(zz,1,cut, seq(0,max(zz, na.rm=TRUE),50), labels=0:(length(seq(0,max(zz, na.rm=TRUE),50))-2))
cuts <- as.data.frame(table(cuts))
cuts[2] = (cuts[2]/sum(cuts[2]))*100

#rounding numerals function
rf = function(x){
  return(format(round(x,2),nsmall=2))
}

#regression function 
lm_eqn <- function(x,y,data)
{
  m <- lm(y ~ x, data);
  eq <- substitute(italic(y) == a + b %.% italic(x)*","~~italic(r)^2~"="~r2, 
                   list(a = format(coef(m)[1], digits = 2), 
                        b = format(coef(m)[2], digits = 2), 
                        r2 = format(summary(m)$r.squared, digits = 3)))
  as.character(as.expression(eq));                 
}

line <-  lm(df2[,3] ~ df2[,2])  # y~ x

int= as.numeric(format(coef(line)[1]))  #intercept
s=as.numeric(format(coef(line)[2]))  #gradient

#coerce factors to number without changing the values

coerce_num <- function(x)
{
  v1 <- as.numeric(as.character(x))
  return(v1)
}