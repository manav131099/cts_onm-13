source('C:/Users/talki/Desktop/cec intern/codes/713/[713] Data_extract.R')
rm(list=ls(all =TRUE))
library(ggplot2)

pathWrite <- "C:/Users/talki/Desktop/cec intern/results/IN-713S/"
result <- read.csv("C:/Users/talki/Desktop/cec intern/results/IN-713S/[IN-713]_summary.csv")
dya <- "C:/Users/talki/Desktop/cec intern/results/IN-713S/[IN-713]doubleyaxisplot1.pdf"
dyr <- "C:/Users/talki/Desktop/cec intern/results/IN-713S/[IN-713]doubleyaxisratio.pdf"

#manually add in x axis labels for last 2 graphs (every 3 months)
monthslabel <- c("Mar","Jun","Sep",'Dec','Mar','Jun')

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
result <- result[,-1]
colnames(result) <- c("date","da","pts","gsi","tamb","hamb","ratio1","ratio2")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))
result[,7] <- as.numeric(paste(result[,7]))
result[,8] <- as.numeric(paste(result[,8]))


date <- result[,1]
last <- tail(result[,1],1)    #date[length(date)] works
first <- date[1]   #date[1]
LTA <- 1939
LTD <- 5.31
dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
da1 <- dagraph + geom_line(size=0.5)
da1 <- da1 + theme_bw()
da1 <- da1 + expand_limits(x=date[1],y=0)
da1 <- da1 + scale_y_continuous(breaks=seq(0, 115, 10))
da1 <- da1 + scale_x_date(date_breaks = "3 month",date_labels = "%b")
da1 <- da1 + ggtitle(paste("[IN-713] Delhi-Data Availability"), subtitle = paste0("From ",date[1]," to ",last))
da1 <- da1 + theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
da1 <- da1 + theme(axis.title.x = element_blank())
da1 <- da1 + theme(axis.text.x = element_text(size=11))
da1 <- da1 + theme(axis.text.y = element_text(size=11))
da1 <- da1 + theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                  plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))
da1 <- da1 + annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size= 3.7,
           x = as.Date(date[round(0.5*length(date))]), y= 112)
da1 <- da1 + annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 3.7,
           x = as.Date(date[round(0.5*length(date))]), y= 107)
da1 <- da1 + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

da1

ggsave(da1,filename = paste0(pathWrite,"IN-713_DA_LC.pdf"), width = 7.92, height = 5)


gsiGraph <- ggplot(result, aes(x=date,y=gsi))+ylab("GHI [kW/m�]")
gs1 <- gsiGraph + geom_bar(stat = "identity", width = 1, position = "dodge")
gs1 <- gs1 + theme_bw()
gs1 <- gs1 + expand_limits(x=date[1],y=9)
gs1 <- gs1 + scale_y_continuous(breaks=seq(0, 9, 1))
gs1 <- gs1 + scale_x_date(date_breaks = "3 month",date_labels = "%b")
gs1 <- gs1 + ggtitle(paste("[IN-713] Global Horizontal Irradiation Daily"), subtitle = paste0("From ",date[1]," to ",last))
gs1 <- gs1 + theme(axis.title.y = element_text(face = "bold",size = 12,margin = margin(0,2,0,0)))
gs1 <- gs1 + theme(axis.title.x = element_blank())
gs1 <- gs1 + theme(axis.text.x = element_text(size=11))
gs1 <- gs1 + theme(axis.text.y = element_text(size=11))
gs1 <- gs1 + theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                     plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))
gs1 <- gs1 + geom_hline(yintercept=LTD, size=0.3, color ="blue")
gs1 <- gs1 + annotate("text",label = paste0("Long-term average annual GHI = ",LTA," kWh/m�   "),size= 3.7,
           x = as.Date(date[round(0.518*length(date))]), y= 8.5)
gs1 <- gs1 + annotate("text",label = paste0("Long-term average daily GHI =",LTD," kWh/m�.day"),size= 3.7,
           x = as.Date(date[round(0.518*length(date))]), y= 8.0, color="blue")
gs1 <- gs1 + annotate("text",label = paste0("Current average daily GHI = ",round(mean(result[,4]),2)," kWh/m�.day      "),size= 3.7,
           x = as.Date(date[round(0.518*length(date))]), y= 7.5, color="red")
gs1 <- gs1 + geom_hline(yintercept=round(mean(result[,4]),2), size=0.3, color="red")
gs1 <- gs1 + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))

gs1

ggsave(gs1,filename = paste0(pathWrite,"IN-713_GSI_LC.pdf"), width = 7.92, height = 5)

tamb <- as.numeric(paste(result[,5]))
hamb <- as.numeric(paste(result[,6]))
pdf(dya, width =7.92, height=5)
yaxis <- tamb
yaxis2 <- hamb
xaxis <- c(1:length(yaxis))
#xaxis2 <- seq(15,(length(yaxis)),30)  # here whats need to change if labels length differ error
xaxis2 <- seq(46,length(yaxis),91)  #this for xlabels in every 3 months

b_unit <- expression(~degree~C) 

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, tamb, pch=4, axes=FALSE, ylim=c(-100,75), xlab="", ylab="", 
     type="l",col="orange", main=" ")
axis(2, ylim=c(-100,75),at = seq(-100,75,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Tamb [ C]")),side=2,line=2.5,cex=1,las =3)
#text(-27,35,b_unit,xpd=NA,srt = 90, cex = 1.1)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, hamb, pch=3,  xlab="", ylab="", ylim=c(-0,250), 
     axes=FALSE, type="l", col="blue")

axis(4, ylim=c(-0,250), col="black",col.axis="black",las=1)
text(length(xaxis)*1.15,125,expression(paste(bold("Hamb [%]"))),xpd=NA,srt = -90, cex = 1)

title(paste("[IN-713] Daily Average Ambient Temperature and Relative Humidity \n From ", date[1], " to ", last), col="black", cex.main =1)

#mtext(expression(paste(bold("Daily Average Ambient Temperature and Relative Humidity"))),side=3,col="black",line=1.75,cex=1.2)  
#mtext(expression(paste(bold("from 2016-03-18 to ", last))),side=3,col="black",line=0.75,cex=1.2)  
text(length(xaxis)/2, 225, paste("Average Tamb =",format(round(mean(tamb[abs(tamb)<100]),1),nsmall = 1),"C"),cex = .8,col = "orange")
text(length(xaxis)/2, 25, paste("Average Hamb =",format(round(mean(hamb[abs(hamb)<100]),1),nsmall = 1),"%"),cex = .8,col = "blue")

#axis(1, at = xaxis2, cex.axis = 0.65, labels = c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov",'Dec','Jan'))
axis(1, at = xaxis2, cex.axis = 0.65, labels = monthslabel)
legend(length(yaxis)*0.85,260,
       c("Tamb","Hamb"),
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("orange","blue")) # gives the legend lines the correct color and width

par(new=TRUE)


dev.off()

ratio1 <- as.numeric(paste(result[,7]))
ratio2 <- as.numeric(paste(result[,8]))
ratio1 <- ratio1[-(1:2)]
ratio2 <- ratio2[-(1:2)]
#pdf("C:/Users/talki/Desktop/CleanTech/result/713/713doubleyaxisratio.pdf",width=7.92,height=5)
pdf(dyr,width=7.92, height=5)
yaxis <- ratio1
yaxis2 <- ratio2
xaxis <- c(1:length(yaxis))
#xaxis2 <- seq(15,(length(yaxis)),30)  #prev is 30.5/mnth
#b_unit <- expression(~degree~C) 

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, ratio1, pch=4, axes=FALSE, ylim=c(0,150), xlab="", ylab="", 
     type="l",col="blue", main=" ")
axis(2, ylim=c(0,150),at = seq(0,150,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Pyr/Gsi02 Ratio [%]")),side=2,line=2.5,cex=1,las =3)
#text(-27,35,b_unit,xpd=NA,srt = 90, cex = 1.1)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, ratio2, pch=3,  xlab="", ylab="", ylim=c(50,200), 
     axes=FALSE, type="l", col="orange")

axis(4, ylim=c(50,200), at = seq(-50,200,25),col="black",col.axis="black",las=1)
#text(length(xaxis)+1450,100,expression(paste(bold(""))),xpd=NA,srt = -90, cex = 1.2)
text(length(xaxis)*1.15,125,expression(paste(bold("Gsi01/Gsi02 Ratio [%]"))),xpd=NA,srt = -90, cex = 1)

title(paste("[IN-713] Pyr/Gsi02 & Gsi01/Gsi02 Ratio Plot \n From ", date[1], " to ", last), col="black", cex.main =1)

text(length(xaxis)/2, 180, paste("Average Pyr/Gsi02 ratio =",format(round(mean(ratio1),1),nsmall =1),"%"),cex = .8,col="blue")
text(length(xaxis)/2, 60, paste("Average Gsi01/Gsi02 ratio =",format(round(mean(ratio2),1),nsmall = 1),"%"),cex = .8,col = "orange")

#axis(1,at = xaxis2,cex.axis = 0.6,labels = c("Apr", "May", "Jun", "Jul", "Aug", "Sep","Oct","Nov","Dec","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug"))
#if axis labels disappear; probably too long so make it smaller

axis(1, at = xaxis2, cex.axis = 0.65, labels = monthslabel)

legend(length(yaxis)*0.77,208,
       c("Pyr/Gsi02","Gsi01/Gsi02"),# puts text in the legend
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("blue","orange")) # gives the legend lines the correct color and width

par(new=TRUE)

dev.off()

print(paste0("Graphs located @  ",pathWrite))