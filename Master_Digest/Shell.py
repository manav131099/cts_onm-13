import os
import glob
import pandas as pd
import numpy as np
from numpy import *
import statistics
import math

import os
os.unlink("//home/admin/Dropbox/MASTER_DIGEST/Shell_Master.txt")

from datetime import date
today = str(date.today())
year = today[0:4]
content = []
count_pr = 0
count_pr_b1 = 0
count_pr_b2 = 0
count_energy = 5 #CHANGE LATER
month = today[0:7]


site_id = ['MY-401L', 'MY-402L','MY-403L', 'MY-404L','MY-405L', 'MY-406L', 'MY-408L','MY-409L', 'MY-410L','MY-411L', 'MY-412L','MY-413L', 'MY-414L','MY-415L', 'MY-416L','MY-417L', 'MY-418L','MY-419L']
source = ['MY-404L', 'MY-402L','MY-402L','MY-404L','MY-405L', 'MY-406L','MY-402L','MY-402L','MY-410L','MY-404L','MY-402L','MY-410L','MY-404L','MY-404L','MY-410L','MY-404L','MY-404L','MY-404L']
cap = [70.875, 70.875, 58.32, 70.875, 64.8, 70.875, 29.565, 44.55, 50.95, 41.31, 53.865, 33.21, 71.76, 71.1, 40.95, 33.3 , 35.55, 71.1 ]
names = ['Shell Putra Heights, USJ','Shell Presint 18, Putrajaya','Shell, Cyberjaya','Shell Mint Hotel','Shell Alor Pongsu Layby NB','Shell Hentian Tangkak','Shell Jalan Puchong (Putra Perdana)','Shell Bandar Puteri Puchong','Shell Jalan Subang Sg Buloh','Shell Kajang Bypass','Shell Skve Serdang Kajang-bound','Shell Bandar Saujana Utama','Shell NSE Bukit Jalil (TPM 2)','Shell Desa Petaling','Shell Jln Ipoh Sentul', 'Shell Jenjarom Mukim Tanjung 12','Shell Bandar Kinrara 5','Shell Elite Highway']
meter_name = ['MFM_1_PV Meter', 'MFM_1_PV Meter','MFM_1_PV Meter', 'MFM_1_PV Meter','MFM_1_PV Meter', 'MFM_1_PV Meter','MFM_1_PV MFM', 'MFM_1_PV MFM','MFM_1_PV MFM', 'MFM_1_PV MFM','MFM_1_PV MFM', 'MFM_1_PV MFM','MFM_1_PV MFM', 'MFM_1_PV MFM','MFM_1_PV MFM', 'MFM_1_PV MFM','MFM_1_PV MFM','MFM_1_PV Meter']
tot_en = []
tot_yld = []
tot_pr = []
tot_irr = []


for a, b, d in zip(site_id, source, meter_name):
  path = '/home/admin/Dropbox/Second Gen/['+a+']/'+year+'/'+month+'/'+d+'/['+a+']-MFM1-'+today+'.txt'
  df_mfm = pd.read_csv(path,sep='\t',header=1,skipinitialspace=True)
  df_mfm = df_mfm.columns.tolist()
  tot_en.append(df_mfm[2])
  tot_yld.append(df_mfm[4])
  tot_pr.append(df_mfm[6])
  path_wms = '/home/admin/Dropbox/Second Gen/['+b+']/'+year+'/'+month+'/WMS_1_Sensor/['+b+']-WMS1-'+today+'.txt'
  df_wms = pd.read_csv(path_wms,sep='\t',header=1,skipinitialspace=True)
  df_wms = df_wms.columns.tolist()
  if(a == "MY-417L" or a == "MY-419L"):
    tot_irr.append(0)
  else:
    tot_irr.append(df_wms[2])

tot_en = [0 if x.startswith("NA") else x for x in tot_en]
tot_yld = [0 if x.startswith("NA") else x for x in tot_yld] 
tot_pr = [0 if x.startswith("NA") else x for x in tot_pr]

tot_en = list(map(float, tot_en))


tot_yield = round(sum(tot_en)/983.83,2)

tot_yld = list(map(float, tot_yld))
std_yld = statistics.stdev(tot_yld)
yld_mean = statistics.mean(tot_yld) 
yld_cov = round(std_yld/yld_mean,1)
yld_cov = yld_cov*100



tot_irr = list(map(float, tot_irr))
std_irr = statistics.stdev(tot_irr)
irr_mean = round(statistics.mean(tot_irr),2) 
irr_cov = round(std_irr/irr_mean,1)
irr_cov = irr_cov*100

tot_pr = list(map(float, tot_pr))
count_pr_b1 = 0
count_pr = 0 # CHANGE LATER
cpunt_pr_b2 = 0
count_energy = 0
for i in tot_pr:
  if(i>80):
    count_pr_b1 = count_pr_b1+1
for i in tot_pr:
  if(i<=70):
    count_pr = count_pr+1
for i in tot_pr:
  if(i>70 and i<=80):
    count_pr_b2 = count_pr_b2+1

for i in tot_en:
  if(i<=0):
    count_energy = count_energy+1

tot_en_sum = round(sum(tot_en),2)
est_PR = round(tot_yield*100/irr_mean,1)


writepath = '/home/admin/Dropbox/MASTER_DIGEST/Shell_Master.txt'
target = open(writepath, 'a+')
   
target.write("SHELL MASTER DIGEST")
target.write("\r\n________________________________________________________________________________\r\n\n")
    #target.write("Sites: TH-010L, TH-011L, TH-012L, TH-013L, TH-014L, TH-015L, TH-010L, TH-011L, TH-012L, TH-013L, TH-014L, TH-015L"+'\n\n')
#target.write("Total Installed Capacity [kWp]: 799.23"+'\n\n')
target.write("Total Number of Running Sites: 18"+'\n\n')
target.write("Number of sites with irradiance sensors: 5"+'\n\n') 

target.write("Total Energy Generated [kWh]: ")
target.write(str(tot_en_sum))
target.write('\n\n')
target.write("Total Yield [kWh/kWp]: ")
target.write(str(tot_yield)+'\n\n')
target.write("COV Yields [%]: ")
target.write(str(yld_cov)+'\n\n')
target.write("Average Irradiation at sites with sensors [kWh/m2]: ")
target.write(str(irr_mean)+'\n\n')
target.write("COV Irradiation [%]: ")
target.write(str(irr_cov)+'\n\n')
target.write("Estimated Portfolio PR [%]: ")
target.write(str(est_PR)+'\n\n')
target.write("Number of Sites with PR > 80%: ")
target.write(str(count_pr_b1)+'\n\n')
target.write("Number of Sites with 70 < PR < 80: ")
target.write(str(count_pr_b2)+'\n\n')
target.write("Number of Sites with PR < 70%: ")
target.write(str(count_pr)+'\n\n')
target.write("Number of Sites with No Generation: ")
target.write(str(count_energy)+'\n')

for a,b,c,d,e,n in zip(site_id,tot_en, tot_yld, tot_pr, tot_irr, names):
  target.write("\r\n________________________________________________________________________________\r\n")
  target.write("["+a+"]  - "+n)
  target.write("\r\n________________________________________________________________________________\r\n")
  target.write('\n')
  target.write("Installed Capacity [kWp]: 70.875"+'\n\n')
  target.write("Energy Generated [kWh]: ")
  target.write(str(b)+ '\n\n')
  target.write("Yield [kWh/kWp]: ")
  target.write(str(c)+ '\n\n')
  target.write("Performance Ratio [%]: ")
  target.write(str(d)+ '\n\n')
  target.write("Irradiation [kWh/m2]: ")
  target.write(str(e)+ '\n')



